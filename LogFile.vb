Imports System.Text
Imports System.IO

Public Class LogFile
    Private Const DOTTED_LINE As String = "-------------------------------------------------------------------------------"
    Private Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Private Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Private Shared myEncoding As Encoding = Encoding.GetEncoding(1252)
    'Private Shared myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")

    Public Shared Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"

        Dim strLine As String
        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.StackTrace & vbCrLf
        strLine &= DOTTED_LINE

        On Error Resume Next
        Dim w As StreamWriter = New StreamWriter(strFile, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"

        Dim strLine As String = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding) '  create a Char writer 
        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding) '  create a Char writer 
        w.WriteLine(strMessage)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String)
        Dim w As StreamWriter = New StreamWriter(strFileName, True, myEncoding) '  create a Char writer 
        w.WriteLine(strContent)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, ByVal append As Boolean)
        Dim w As StreamWriter = New StreamWriter(strFileName, append, myEncoding) '  create a Char writer 
        w.WriteLine(strContent)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As StreamReader = New StreamReader(strFileName, myEncoding) ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Shared Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

End Class

