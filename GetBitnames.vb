Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationSettings
Imports System.Xml

Public Class GetBitnames

    ' All Keys are converted to LowerCase and are looked up as LowerCase to avoid confusion of mixed cases

    Private htBitCat As Hashtable
    Private htBitSubcat As Hashtable
    Private htXPath As Hashtable
    Private htKeywords As Hashtable
    Private htNtbFolder As Hashtable
    Private htImportSQL As Hashtable
    Private htImportAutonomy As Hashtable
    Private cn As SqlConnection

    Public Sub Init(ByVal strConnectionString As String)
        cn = New SqlConnection()

        If strConnectionString = "" Then
            cn.ConnectionString = AppSettings("ConnectionString")
        Else
            cn.ConnectionString = strConnectionString
        End If

        Try
            cn.Open()

            InitBitnames(cn)
            InitXPath(cn)
            InitKeywords(cn, True)
            InitNtbFolder(cn)
        Catch e As Exception
            Debug.WriteLine(e.Message & vbCrLf & e.StackTrace)
            cn.Close()
        End Try

        cn.Close()
        ' Close the connection when done with it.
    End Sub

    Public Sub RefreshKeywords()
        cn.Open()
        InitKeywords(cn, False)
        cn.Close()
    End Sub

    Private Sub InitBitnames(ByRef cn As SqlConnection)
        htBitCat = New Hashtable()
        htBitSubcat = New Hashtable()

        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        mySqlCommand = New SqlCommand("getBitnames", cn)

        myDataReader = mySqlCommand.ExecuteReader()

        Dim strKey As String
        Dim strKeySub As String
        Dim intCategory As Integer
        Dim intSubcat As Int64
        Dim strXPath As String

        '*** Fill Bitmaps from table BITNAMES: for Category and Subcategory into Hastables
        Do While (myDataReader.Read())

            'strKey = myDataReader.GetByte(2).ToString & myDataReader.GetString(6)
            strKey = LCase(myDataReader.GetString(6))
            intCategory = myDataReader.GetInt32(3)
            intSubcat = myDataReader.GetInt64(4)

            If intSubcat = 0 Then
                htBitCat.Add(strKey, intCategory)
            Else
                strKeySub = strKey '& intCategory.ToString
                htBitSubcat.Add(strKeySub, intSubcat)
            End If
        Loop

        ' Always call Close when done reading.
        myDataReader.Close()
    End Sub

    Private Sub InitXPath(ByRef cn As SqlConnection)
        Dim strKey As String
        Dim strXPath As String
        htXPath = New Hashtable()
        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        mySqlCommand = New SqlCommand("getTypeOfName", cn)
        myDataReader = mySqlCommand.ExecuteReader()

        '*** Fill XPath expression from table TypeOfNames into Hastables
        Do While (myDataReader.Read())

            strKey = LCase(myDataReader.GetString(1))
            strXPath = myDataReader.GetString(2) & ""

            htXPath.Add(strKey, strXPath)
        Loop

        myDataReader.Close()
    End Sub

    Private Sub InitKeywords(ByRef cn As SqlConnection, ByVal blnNew As Boolean)
        If blnNew Then
            htKeywords = New Hashtable()
        Else
            htKeywords.Clear()
        End If

        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        mySqlCommand = New SqlCommand("getKeywords", cn)
        myDataReader = mySqlCommand.ExecuteReader()

        Dim strKey As String
        Dim intID As Integer

        '*** Fill Bitmaps for Keywords into Hastables
        Do While (myDataReader.Read())

            intID = myDataReader.GetInt32(0)
            strKey = LCase(myDataReader.GetString(1))

            htKeywords.Add(strKey, intID)
        Loop

        ' Always call Close when done reading.
        myDataReader.Close()

    End Sub

    Private Sub InitNtbFolder(ByRef cn As SqlConnection)
        htNtbFolder = New Hashtable()
        htImportSQL = New Hashtable()
        htImportAutonomy = New Hashtable()

        Dim myDataReader As SqlDataReader
        Dim mySqlCommand As SqlCommand

        mySqlCommand = New SqlCommand("getNtbFolder", cn)
        myDataReader = mySqlCommand.ExecuteReader()

        Dim strKey As String
        Dim intID As Integer
        Dim importSQL As Boolean
        Dim importAutonomy As Boolean

        '*** Fill Bitmaps for Keywords into Hastables
        Do While (myDataReader.Read())
            intID = myDataReader.GetInt32(0)
            strKey = LCase(myDataReader.GetString(1))
            importSQL = myDataReader.GetBoolean(2)
            importAutonomy = myDataReader.GetBoolean(3)
            htNtbFolder.Add(strKey, intID)
            htImportSQL.Add(strKey, importSQL)
            htImportAutonomy.Add(strKey, importAutonomy)
        Loop

        ' Always call Close when done reading.
        myDataReader.Close()

    End Sub

    Public Function GetBitmap(ByRef strNITFCont As String, Optional ByRef isHex As Boolean = False) As String
        Dim intBitmap As Integer
        intBitmap = htBitCat.Item(LCase(strNITFCont))
        If isHex Then
            Return Hex(intBitmap)
        Else
            Return intBitmap
        End If
    End Function

    Public Function GetBitmap(ByRef xmlCatNodeList As XmlNodeList, Optional ByRef isHex As Boolean = False) As String
        Dim xmlCatNode As XmlNode
        Dim intBitmap As Integer
        For Each xmlCatNode In xmlCatNodeList
            intBitmap = intBitmap Or htBitCat.Item(LCase(xmlCatNode.InnerText))
        Next
        If isHex Then
            Return Hex(intBitmap)
        Else
            Return intBitmap
        End If
    End Function

    Public Function GetBitmap(ByRef strArray() As String, Optional ByRef isHex As Boolean = False) As String
        Dim intBitmap As Integer
        Dim strTemp As String

        For Each strTemp In strArray
            If strTemp <> "" Then
                intBitmap = intBitmap Or htBitCat.Item(LCase(strTemp))
            End If
        Next

        If isHex Then
            Return Hex(intBitmap)
        Else
            Return intBitmap
        End If
    End Function

    Public Function GetBitmapSub(ByRef strNITFCont As String) As Int64
        Dim intBitmap As Int64
        intBitmap = htBitSubcat.Item(LCase(strNITFCont))
        Return intBitmap
    End Function

    Public Function GetBitmapSub(ByRef strArray() As String, Optional ByRef isHex As Boolean = False) As String
        Dim intBitmap As Int64
        Dim strTemp As String

        For Each strTemp In strArray
            If strTemp <> "" Then
                'BUGFIX:
                If strTemp = "ěkonomisk politikk" Then strTemp &= "/skatt"
                intBitmap = intBitmap Or htBitSubcat.Item(LCase(strTemp))
            End If
        Next
        If isHex Then
            Return Hex(intBitmap)
        Else
            Return intBitmap
        End If
    End Function

    Public Function GetBitmapSub(ByRef nodes As XmlNodeList, Optional ByRef isHex As Boolean = False) As String
        Dim intBitmap As Int64
        Dim nd As XmlNode

        For Each nd In nodes
            If nd.InnerText <> "" Then

                intBitmap = intBitmap Or htBitSubcat.Item(LCase(nd.InnerText))
            End If
        Next
        If isHex Then
            Return Hex(intBitmap)
        Else
            Return intBitmap
        End If
    End Function

    Public Function GetNitfXPath(ByRef strTypeText As String) As String
        Dim strNitfXPath As String
        strNitfXPath = htXPath.Item(LCase(strTypeText))
        Return strNitfXPath
    End Function

    Public Function GetKeywordID(ByRef Key As String) As Integer
        Dim intKeywordID As Integer
        'Dim strKeyword As String
        Dim intEnds As Integer

        If Key.StartsWith("HAST-") Then
            Key = Key.Substring(5)
        ElseIf Key.StartsWith("FAKTA-") Then
            Key = Key.Substring(6)
        End If

        intEnds = Key.IndexOf("-")
        If intEnds > 1 Then
            Key = Key.Substring(0, intEnds)
            'Else
            '   strKeyword = Key
        End If

        intKeywordID = htKeywords.Item(LCase(Key))
        Return intKeywordID
    End Function

    Public Function GetNtbFolderID(ByRef Key As String) As Integer
        Return htNtbFolder.Item(LCase(Key))
    End Function

End Class


