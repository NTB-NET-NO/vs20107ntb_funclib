Imports System.Xml
Imports System.IO
' Library of Common functions For Portal Project in VB .NET

Public Class FuncLib

    Public Shared Function MakeSubDir(ByRef strFilePath As String, ByVal blnCreateDirectory As Boolean) As String
        Dim strFile As String = Path.GetFileNameWithoutExtension(strFilePath)
        Dim strPath As String = Path.GetDirectoryName(strFilePath)
        Dim strSub As String

        Dim intLen As Integer = strFile.Length

        Dim i As Integer
        For i = 1 To 2
            strSub &= "sub" & strFile.Substring(intLen - i, 1) & "\"
        Next

        strPath = strPath & "\" & strSub
        If blnCreateDirectory Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    Public Shared Function MakeSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        Dim strFile As String = Path.GetFileNameWithoutExtension(strFilePath)
        Dim strPath As String = Path.GetDirectoryName(strFilePath)
        Dim strSub As String

        'strSub &= dtTimeStamp.Year & "\" & dtTimeStamp.Month & "-" & dtTimeStamp.Day
        strSub &= Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")
        strPath = strPath & "\" & strSub & "\"
        If Not Directory.Exists(strPath) Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    Public Shared Function GetSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        Dim strFile As String = Path.GetFileNameWithoutExtension(strFilePath)
        Dim strPath As String = Path.GetDirectoryName(strFilePath)
        Dim strSub As String

        'strSub &= dtTimeStamp.Year & "\" & dtTimeStamp.Month & "-" & dtTimeStamp.Day
        strSub &= Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")
        strPath = strPath & "\" & strSub & "\"

        Return strPath & Path.GetFileName(strFilePath)

    End Function

End Class
