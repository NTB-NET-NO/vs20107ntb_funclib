' Functions to generate IDX files for Autonomy
' Created by Roar Vestre 2002
' Last change 2002.11.13 Roar Vestre
'

Imports System.Xml
Imports System.Text
Imports System.IO
Imports ntb_FuncLib

Public Class AutonomyIDX
    Private xmlDoc As XmlDocument
    Private sw As StreamWriter
    Dim objBitNames As GetBitnames

    Public Sub New()
        ' Void
    End Sub

    Public Sub New(ByVal strConnectionString As String)
        objBitNames = New GetBitnames()
        objBitNames.Init(strConnectionString)
    End Sub

    ' Open and create empty IDX-File
    Public Sub Open(ByVal strIdxFile As String)
        sw = New StreamWriter(strIdxFile, False, System.Text.Encoding.GetEncoding("iso-8859-1"))    ' create a stream writer 
    End Sub

    ' Flush and Close IDX-File
    Public Sub Close()
        xmlDoc = Nothing
        sw.Flush()
        sw.Close()
    End Sub

    ' Write IDX-documents (one or many) to open IDX-File. Must call Close when finished!
    Public Function MakeIdx(ByVal strDbName As String, ByVal intArticleId As Integer, ByRef strXml As String, Optional ByVal bArkiv As Boolean = True) As Boolean
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim Ingress As String = ""
        Dim strAutonomyEtc As String = ""

        On Error Resume Next
        xmlDoc.LoadXml(strXml)
        If Err.Number <> 0 Then
            Exit Function
        End If
        On Error GoTo 0

        Dim ArticleTitle As String = xmlDoc.SelectSingleNode("nitf/body/body.head/hedline/hl1").InnerText.Replace(vbCrLf, " ")

        Dim node As XmlNode = xmlDoc.SelectSingleNode("nitf/body/body.content/p")
        If Not node Is Nothing Then
            Ingress = node.InnerText.Replace(vbCrLf, " ")
        Else
            Ingress = ""
        End If

        Dim xmlNodeListByline As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.head/byline/p")
        For Each node In xmlNodeListByline
            strAutonomyEtc &= node.InnerText & "; "
        Next

        Dim CreationDateTime As DateTime

        Dim strTemp As String = xmlDoc.SelectSingleNode("nitf/head/pubdata/@date.publication").InnerText
        Dim strTemp2 As String = strTemp.Substring(0, 4) & "." & strTemp.Substring(4, 2) & "." & strTemp.Substring(6, 2) & _
                " " & strTemp.Substring(9, 2) & ":" & strTemp.Substring(11, 2) & ":" & strTemp.Substring(13, 2)
        CreationDateTime = DateTime.Parse(strTemp2)

        Dim strMaingroup As String = ""
        Dim strSubgroup As String = ""
        Dim strCat As String = ""

        Dim strSubKul As String = ""
        Dim strSubKur As String = ""
        Dim strSubOko As String = ""
        Dim strSubSpo As String = ""

        Dim bKul As Boolean
        Dim bKur As Boolean
        Dim bOko As Boolean
        Dim bSpo As Boolean

        Dim strEvloc As String = ""
        Dim strCounty As String = ""
        Dim strSakID As String = ""
        Dim strUrgency As String = ""

        ' Get the other values from the NITF xml-document
        On Error Resume Next
        strMaingroup = xmlDoc.SelectSingleNode("nitf/head/tobject/@tobject.type").InnerText
        strSubgroup = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.property/@tobject.property.type").InnerText
        strSakID = xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-id']/@content").InnerText
        strUrgency = xmlDoc.SelectSingleNode("nitf/head/docdata/urgency/@ed-urg").InnerText

        bKul = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUL']").Count
        bKur = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUR']").Count
        bOko = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='OKO']").Count
        bSpo = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']").Count

        If bKul Then strSubKul = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter").InnerText
        If bKur Then strSubKur = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter").InnerText
        If bOko Then strSubOko = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.subject[@tobject.subject.code='OKO']/@tobject.subject.matter").InnerText
        If bSpo Then strSubSpo = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter").InnerText

        strEvloc = xmlDoc.SelectSingleNode("nitf/head/docdata/evloc/@state-prov").InnerText
        strCounty = xmlDoc.SelectSingleNode("nitf/head/docdata/evloc/@county-dist").InnerText

        strAutonomyEtc &= strEvloc & " "
        strAutonomyEtc &= strCounty & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-klasse']/@content").InnerText & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-omr�de']/@content").InnerText & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-emne']/@content").InnerText & " "
        On Error GoTo 0

        If strSakID = "" Then
            strSakID = "VOID-" & intArticleId
        ElseIf strUrgency < "4" And bArkiv Then
            strSakID = "HAST-" & strSakID
        End If

        ' Make HEX bitmaps for BITAND search 
        strMaingroup = objBitNames.GetBitmap(strMaingroup, True)
        strSubgroup = objBitNames.GetBitmap(strSubgroup, True)

        If strEvloc.ToLower().IndexOf("norge") > -1 Then
            If strEvloc <> "" Then strEvloc = objBitNames.GetBitmap(Split(strEvloc, ";"), True)
            If strCounty <> "" Then strCounty = objBitNames.GetBitmapSub(Split(strCounty, ";"), True)
        ElseIf strEvloc <> "" Then
            strEvloc = objBitNames.GetBitmap(Split(strEvloc, ";"), True)
            strCounty = "FFFFFF"
        End If

        ' Make a list of categories.
        strCat = objBitNames.GetBitmap(xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject/@tobject.subject.code"), True).ToString

        ' Make a list of Sub Categories.
        If strSubKul <> "" Then
            strSubKul = objBitNames.GetBitmapSub(Split(strSubKul, ";"), True)
        ElseIf Not bKul Then
            strSubKul = "FF"
        End If

        If strSubKur <> "" Then
            strSubKur = objBitNames.GetBitmapSub(Split(strSubKur, ";"), True)
        ElseIf Not bKur Then
            strSubKur = "F"
        End If

        If strSubOko <> "" Then
            strSubOko = objBitNames.GetBitmapSub(Split(strSubOko, ";"), True)
        ElseIf Not bOko Then
            strSubOko = "FFFF"
        End If

        If strSubSpo <> "" Then
            strSubSpo = objBitNames.GetBitmapSub(Split(strSubSpo, ";"), True)
        ElseIf Not bSpo Then
            strSubSpo = "FFFFFFFFFFFFFFFF"
        End If

        ' Make a list of sport names in body text for word searches.
        Dim xmlNodeListSubCat As XmlNodeList = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter")
        Dim xmlSubCat As XmlNode
        For Each xmlSubCat In xmlNodeListSubCat
            strAutonomyEtc &= xmlSubCat.InnerText
        Next

        'Autonomy IDX-file
        Dim strDreSummary As String = "#DREFIELD summary=""" & Ingress.Replace("""", "&quot;") & """" & vbCrLf

        Dim strDreHead As String
        strDreHead &= "#DREREFERENCE " & Trim(intArticleId.ToString) & vbCrLf
        strDreHead &= "#DRETITLE " & ArticleTitle & vbCrLf

        strDreHead &= "#DREFIELD CreationDateTime=""" & Format(CreationDateTime, "dd.MM.yyyy HH:mm") & """" & vbCrLf

        Dim DreDate As String = Format(CreationDateTime, "yyyy/MM/dd/HH/mm").Replace(".", "/")
        strDreHead &= "#DREFIELD DREDATE=""" & DreDate & """" & vbCrLf

        strDreHead &= "#DREFIELD Urgency=""" & strUrgency & """" & vbCrLf
        strDreHead &= "#DREFIELD Maingroup=""" & strMaingroup & """" & vbCrLf
        strDreHead &= "#DREFIELD Subgroup=""" & strSubgroup & """" & vbCrLf

        strDreHead &= "#DREFIELD SakID=""" & strSakID & """" & vbCrLf

        strDreHead &= "#DREFIELD Categories=""" & strCat & """" & vbCrLf
        ' Make a l
        strDreHead &= "#DREFIELD SubKul=""" & strSubKul & """" & vbCrLf
        strDreHead &= "#DREFIELD SubKur=""" & strSubKur & """" & vbCrLf
        strDreHead &= "#DREFIELD SubOko=""" & strSubOko & """" & vbCrLf
        strDreHead &= "#DREFIELD SubSpo=""" & strSubSpo & """" & vbCrLf

        strDreHead &= "#DREFIELD Evloc=""" & strEvloc & """" & vbCrLf
        strDreHead &= "#DREFIELD County=""" & strCounty & """" & vbCrLf

        strDreHead &= "#DREDBNAME " & strDbName & vbCrLf
        strDreHead &= "#DRESTORECONTENT N" & vbCrLf

        'sw.WriteLine(strMaingroup & "; " & strSubgroup & "; " & strAutonomyEtc)
        Dim strBody As String = ""

        Dim blnSubDoc As Boolean
        If xmlDoc.SelectNodes("nitf/body/body.content/hl2").Count > 0 Then
            sw.WriteLine("#DRESECTION 0")
            blnSubDoc = True
        End If
        Dim intSection As Integer = 0
        Dim xmlNodeListBody As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.content/*")
        Dim xmlPar As XmlNode
        For Each xmlPar In xmlNodeListBody
            Select Case xmlPar.Name
                Case "p"
                    If Trim(xmlPar.InnerText) <> "" Then
                        strBody &= xmlPar.InnerText & vbCrLf
                    End If
                Case "hl2"
                    If strBody = "" Then
                        strBody = xmlPar.InnerText & vbCrLf
                    Else
                        sw.WriteLine(strDreHead)
                        sw.WriteLine(strDreSummary)

                        sw.WriteLine("#DRESECTION " & intSection)
                        sw.WriteLine("#DRECONTENT")
                        If strAutonomyEtc <> "" Then
                            sw.WriteLine(strAutonomyEtc)
                        End If
                        sw.WriteLine(strBody)
                        sw.WriteLine("#DREENDDOC")
                        strBody = xmlPar.InnerText & vbCrLf
                        intSection += 1
                    End If
                Case Else

            End Select
        Next

        If strBody = "" Then
            strBody = xmlDoc.SelectSingleNode("nitf/body/body.content").InnerText
        End If

        sw.WriteLine(strDreHead)
        sw.WriteLine(strDreSummary)

        If blnSubDoc = True Then
            sw.WriteLine("#DRESECTION " & intSection)
        End If

        sw.WriteLine("#DRECONTENT")
        If strAutonomyEtc <> "" Then
            sw.WriteLine(strAutonomyEtc)
        End If
        sw.WriteLine(strBody)
        sw.WriteLine("#DREENDDOC")

        sw.Flush()
    End Function

    ' Write IDX-documents (one or many) to open IDX-File. Must call Close when finished!
    Public Function MakeIdx32(ByVal strDbName As String, ByVal intArticleId As Integer, ByRef strXml As String, Optional ByVal bArkiv As Boolean = True) As Boolean
        Dim xmlDoc As XmlDocument = New XmlDocument()
        Dim Ingress As String = ""
        Dim strAutonomyEtc As String = ""

        On Error Resume Next
        xmlDoc.LoadXml(strXml)
        If Err.Number <> 0 Then
            Exit Function
        End If
        On Error GoTo 0

        Dim ArticleTitle As String = xmlDoc.SelectSingleNode("nitf/body/body.head/hedline/hl1").InnerText.Replace(vbCrLf, " ")

        Dim node As XmlNode = xmlDoc.SelectSingleNode("nitf/body/body.content/p")
        If Not node Is Nothing Then
            Ingress = node.InnerText.Replace(vbCrLf, " ")
        Else
            Ingress = ""
        End If

        Dim xmlNodeListByline As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.head/byline/p")
        For Each node In xmlNodeListByline
            strAutonomyEtc &= node.InnerText & "; "
        Next

        Dim CreationDateTime As DateTime
        ' Get DateTime value from NITF field: "nitf/head/docdata/date.issue/@norm"
        CreationDateTime = xmlDoc.SelectSingleNode("nitf/head/docdata/date.issue/@norm").InnerText

        Dim strMaingroup As String = ""
        Dim strSubgroup As String = ""
        Dim strCat As String = ""

        Dim ndSubKul As XmlNodeList
        Dim ndSubKur As XmlNodeList
        Dim ndSubOko As XmlNodeList
        Dim ndSubSpo As XmlNodeList

        Dim strSubKul As String
        Dim strSubKur As String
        Dim strSubOko As String
        Dim strSubSpo As String

        Dim bKul As Boolean
        Dim bKur As Boolean
        Dim bOko As Boolean
        Dim bSpo As Boolean

        Dim ndEvloc As XmlNodeList
        Dim ndCounty As XmlNodeList
        Dim strEvloc As String = ""
        Dim strCounty As String = ""

        Dim strSakID As String = ""
        Dim strUrgency As String = ""

        ' Get the other values from the NITF xml-document
        On Error Resume Next
        strMaingroup = xmlDoc.SelectSingleNode("nitf/head/tobject/@tobject.type").InnerText
        strSubgroup = xmlDoc.SelectSingleNode("nitf/head/tobject/tobject.property/@tobject.property.type").InnerText
        strSakID = xmlDoc.SelectSingleNode("nitf/head/meta[@name='NTBID']/@content").InnerText
        strUrgency = xmlDoc.SelectSingleNode("nitf/head/docdata/urgency/@ed-urg").InnerText

        ndSubKul = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter")
        ndSubKur = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter")
        ndSubOko = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='OKO']/@tobject.subject.matter")
        ndSubSpo = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter")

        bKul = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUL']").Count
        bKur = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='KUR']").Count
        bOko = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='OKO']").Count
        bSpo = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']").Count

        ndEvloc = xmlDoc.SelectNodes("nitf/head/docdata/evloc/@state-prov")
        ndCounty = xmlDoc.SelectNodes("nitf/head/docdata/evloc/@county-dist")

        strAutonomyEtc &= strEvloc & " "
        strAutonomyEtc &= strCounty & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-klasse']/@content").InnerText & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-omr�de']/@content").InnerText & " "
        strAutonomyEtc &= xmlDoc.SelectSingleNode("nitf/head/meta[@name='ntb-emne']/@content").InnerText & " "
        On Error GoTo 0

        If strSakID = "" Then
            strSakID = "VOID-" & intArticleId
        ElseIf strUrgency < "4" And bArkiv Then
            strSakID = "HAST-" & strSakID
        End If

        ' Make HEX bitmaps for BITAND search 
        strMaingroup = objBitNames.GetBitmap(strMaingroup, True)
        strSubgroup = objBitNames.GetBitmap(strSubgroup, True)

        strEvloc = objBitNames.GetBitmap(ndEvloc, True)
        strCounty = objBitNames.GetBitmapSub(ndCounty, True)

        'If strEvloc.ToLower().IndexOf("norge") > -1 Then
        '    If strEvloc <> "" Then strEvloc = objBitNames.GetBitmap(Split(strEvloc, ";"), True)
        '    If strCounty <> "" Then strCounty = objBitNames.GetBitmapSub(Split(strCounty, ";"), True)
        'ElseIf strEvloc <> "" Then
        '    strEvloc = objBitNames.GetBitmap(Split(strEvloc, ";"), True)
        '    strCounty = "FFFFFF"
        'End If

        If 256 And CInt(Int("&H" & strEvloc)) Then
            'Do nothing
        ElseIf strCounty = "0" Then
            strCounty = "FFFFFF"
        End If

        ' Make a list of categories.
        strCat = objBitNames.GetBitmap(xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject/@tobject.subject.code"), True).ToString

        ' Make a list of Sub Categories.
        If bKul Then
            strSubKul = objBitNames.GetBitmapSub(ndSubKul, True)
        ElseIf Not bKul Then
            strSubKul = "FF"
        End If

        If bKur Then
            strSubKur = objBitNames.GetBitmapSub(ndSubKur, True)
        ElseIf Not bKur Then
            strSubKur = "F"
        End If

        If bOko Then
            strSubOko = objBitNames.GetBitmapSub(ndSubOko, True)
        ElseIf Not bOko Then
            strSubOko = "FFFF"
        End If

        If bSpo Then
            strSubSpo = objBitNames.GetBitmapSub(ndSubSpo, True)
        ElseIf Not bSpo Then
            strSubSpo = "FFFFFFFFFFFFFFFF"
        End If

        ' Make a list of sport names in body text for word searches.
        Dim xmlNodeListSubCat As XmlNodeList = xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter")
        Dim xmlSubCat As XmlNode
        For Each xmlSubCat In xmlNodeListSubCat
            strAutonomyEtc &= xmlSubCat.InnerText
        Next

        'Autonomy IDX-file
        Dim strDreSummary As String = "#DREFIELD summary=""" & Ingress.Replace("""", "&quot;") & """" & vbCrLf

        Dim strDreHead As String
        strDreHead &= "#DREREFERENCE " & Trim(intArticleId.ToString) & vbCrLf
        strDreHead &= "#DRETITLE " & ArticleTitle & vbCrLf

        strDreHead &= "#DREFIELD CreationDateTime=""" & Format(CreationDateTime, "dd.MM.yyyy HH:mm") & """" & vbCrLf

        Dim DreDate As String = Format(CreationDateTime, "yyyy/MM/dd/HH/mm").Replace(".", "/")
        strDreHead &= "#DREFIELD DREDATE=""" & DreDate & """" & vbCrLf

        strDreHead &= "#DREFIELD Urgency=""" & strUrgency & """" & vbCrLf
        strDreHead &= "#DREFIELD Maingroup=""" & strMaingroup & """" & vbCrLf
        strDreHead &= "#DREFIELD Subgroup=""" & strSubgroup & """" & vbCrLf

        strDreHead &= "#DREFIELD SakID=""" & strSakID & """" & vbCrLf

        strDreHead &= "#DREFIELD Categories=""" & strCat & """" & vbCrLf
        ' Make a l
        strDreHead &= "#DREFIELD SubKul=""" & strSubKul & """" & vbCrLf
        strDreHead &= "#DREFIELD SubKur=""" & strSubKur & """" & vbCrLf
        strDreHead &= "#DREFIELD SubOko=""" & strSubOko & """" & vbCrLf
        strDreHead &= "#DREFIELD SubSpo=""" & strSubSpo & """" & vbCrLf

        strDreHead &= "#DREFIELD Evloc=""" & strEvloc & """" & vbCrLf
        strDreHead &= "#DREFIELD County=""" & strCounty & """" & vbCrLf

        strDreHead &= "#DREDBNAME " & strDbName & vbCrLf
        strDreHead &= "#DRESTORECONTENT N" & vbCrLf

        'sw.WriteLine(strMaingroup & "; " & strSubgroup & "; " & strAutonomyEtc)
        Dim strBody As String = ""

        Dim blnSubDoc As Boolean
        If xmlDoc.SelectNodes("nitf/body/body.content/hl2").Count > 0 Then
            sw.WriteLine("#DRESECTION 0")
            blnSubDoc = True
        End If
        Dim intSection As Integer = 0
        Dim xmlNodeListBody As XmlNodeList = xmlDoc.SelectNodes("nitf/body/body.content/*")
        Dim xmlPar As XmlNode
        For Each xmlPar In xmlNodeListBody
            Select Case xmlPar.Name
                Case "p"
                    If Trim(xmlPar.InnerText) <> "" Then
                        strBody &= xmlPar.InnerText & vbCrLf
                    End If
                Case "hl2"
                    If strBody = "" Then
                        strBody = xmlPar.InnerText & vbCrLf
                    Else
                        sw.WriteLine(strDreHead)
                        sw.WriteLine(strDreSummary)

                        sw.WriteLine("#DRESECTION " & intSection)
                        sw.WriteLine("#DRECONTENT")
                        If strAutonomyEtc <> "" Then
                            sw.WriteLine(strAutonomyEtc)
                        End If
                        sw.WriteLine(strBody)
                        sw.WriteLine("#DREENDDOC")
                        strBody = xmlPar.InnerText & vbCrLf
                        intSection += 1
                    End If
                Case Else

            End Select
        Next

        If strBody = "" Then
            strBody = xmlDoc.SelectSingleNode("nitf/body/body.content").InnerText
        End If

        sw.WriteLine(strDreHead)
        sw.WriteLine(strDreSummary)

        If blnSubDoc = True Then
            sw.WriteLine("#DRESECTION " & intSection)
        End If

        sw.WriteLine("#DRECONTENT")
        If strAutonomyEtc <> "" Then
            sw.WriteLine(strAutonomyEtc)
        End If
        sw.WriteLine(strBody)
        sw.WriteLine("#DREENDDOC")

        sw.Flush()
    End Function

    Private Function GetCategories(ByRef xmlDoc As XmlDocument, ByRef strAutonomyEtc As String) As Boolean
        '*** Get the String values for Categories and Subcat to strAutonomyEtc
        Dim strNITFContent As String
        Dim strSubcat As String
        Dim strSubcatList As String
        Dim strNITFContDone As String = ""
        Dim i As Integer

        Dim node As XmlNode

        For Each node In xmlDoc.SelectNodes("nitf/head/tobject/tobject.subject")
            strNITFContent = node.Attributes.GetNamedItem("tobject.subject.code").InnerText
            If strNITFContDone.IndexOf(strNITFContent) = -1 Then
                strNITFContDone &= strNITFContent & "; "

                Try
                    strAutonomyEtc &= node.Attributes.GetNamedItem("tobject.subject.type").InnerText & "; " 'For Autonumy
                Catch
                    ' Do nothing
                End Try

                Try
                    strSubcatList = node.Attributes.GetNamedItem("tobject.subject.matter").InnerText
                    strAutonomyEtc &= strSubcatList 'For Autonumy
                Catch
                    'intHasReferences = 0
                End Try
            End If
        Next

        Return True
    End Function

End Class
